<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route ::get('/master', function(){
    return view('adminlte.master');
});

Route::get('/items',function(){
    return view('items.index');
});

Route::get('/',function(){
    return view('tugas.garing');
});

Route::get('/data-table', function(){
    return view('tugas.data-table');
});

Route::get('/postst/create','PostController@create'); 

Route::get('/post', 'PostController@index');
Route::get('/post/create', 'PostController@create');
Route::post('/post', 'PostController@store');
Route::get('/post/{post_id}', 'PostController@show');
Route::get('/post/{post_id}/edit', 'PostController@edit');
Route::put('/post/{post_id}', 'PostController@update');
Route::delete('/post/{post_id}', 'PostController@destroy');